from numpy import ndarray
import numpy as np


class Grid:
    def __init__(self, size: int, game_map: ndarray):
        self.size = size
        self.cells = game_map

    def next_iteration(self):
        cells = np.copy(self.cells)
        for row, _ in enumerate(self.cells):
            for column, cell in enumerate(_):
                value = self.compute_value(row, column)
                if value <= 1 or value >= 4:
                    cells[row, column] = 0
                    continue

                if value == 3:
                    cells[row][column] = 1
                    continue

        self.cells = cells
        return cells

    def compute_value(self, row, col) -> int:
        if (
            row != 0 and
            col != 0 and
            row + 1 < self.size and
            col + 1 < self.size
        ):
            return (
                self.cells[row - 1][col - 1] +
                self.cells[row - 1][col] +
                self.cells[row - 1][col + 1] +
                self.cells[row][col - 1] +
                self.cells[row][col + 1] +
                self.cells[row + 1][col - 1] +
                self.cells[row + 1][col] +
                self.cells[row + 1][col + 1]
            )
        return 0
