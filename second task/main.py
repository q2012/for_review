import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from models.grid import Grid

fig, ax = plt.subplots()
data = np.random.randint(2, size=(100, 100))
im = ax.imshow(data, cmap='gray_r')
grid = Grid(len(data), data)


def game_init():
    return im


def animate(i):
    data = grid.next_iteration()
    im.set_data(data)

    return im


anim = animation.FuncAnimation(
    fig, animate, interval=500, init_func=game_init
)
plt.show()
