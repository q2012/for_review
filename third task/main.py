coin_probabilities = [0.1, 0.2, 0.4, 0.8, 0.9]
coin_pick_chance = [0.2] * 5


# Calculating full probability
def calculate_probability(other_side: bool = False) -> float:
    probability = 0

    for index, prob in enumerate(coin_probabilities):
        _prob = prob
        if other_side:
            _prob = 1 - prob

        probability += _prob * coin_pick_chance[index]

    return round(probability, 2)


# Recalculating coins pick chances according to toss result. Bayes theorem
def recalculate_chances(result: int) -> None:
    full_prob = calculate_probability()
    if not result:
        full_prob = calculate_probability(True)

    for index, chance in enumerate(coin_pick_chance):
        if result:
            coin_pick_chance[index] = (
                  (chance * coin_probabilities[index]) / full_prob
            )
            continue

        coin_pick_chance[index] = (
              (chance * (1 - coin_probabilities[index])) / full_prob
        )


def test_several_tosses(tosses: list[int]):
    result = []
    for toss in tosses:
        recalculate_chances(toss)
        result.append(calculate_probability())

    return result


print(test_several_tosses([1, 1, 1, 0, 1, 0, 1, 1]))
# [0.69, 0.79, 0.84, 0.74, 0.8, 0.7, 0.75, 0.8]
