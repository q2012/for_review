import numpy as np
from numpy import ndarray
from typing import Optional


def solve_equation(
    first_matrix: list,
    result_matrix: list
) -> Optional[ndarray]:
    matrix = np.array(first_matrix)
    result = np.array(result_matrix)

    det = np.linalg.det(matrix)
    if det == 0:
        print('Determinant equals to zero.')
        return None

    inv_matrix = np.linalg.inv(matrix)  # Computing A^(-1)
    solution = np.matmul(inv_matrix, result)  # Finding X

    return solution


if __name__ == '__main__':
    sol = solve_equation(
        [[1, 2, 3],
         [0, 1, 2],
         [2, 0, 0]],
        [[1],
         [1],
         [0]]
    )

    print(sol)  # X^T = [0, -1, 1]
