Solutions to test-tasks from DataScience camp.

First task: matrix equations (AX = B) solver. Could've been done using numpy.solve function.

Second task: Conway's Game of Life implementation with visualisation using matplotlib.

Third task: having 5 coins with different probabilities to land on certain side after toss. We randomly choose 1 coin out of 5 and having a series of 8 tosses have to calculate for each toss the probability that after the next toss coin will land on certain side.
